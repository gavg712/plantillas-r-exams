Lista de ejercicios en este directorio.

- `boxhist`. *Práctico en R*. Pregunta con múltiples enunciados, sobre análisis exploratorio de datos usando boxplots, histogrmas y diagrama de cinta (stripchart). Es la versión en español traducida del ejemplo oficial de [r-exams.org](http://www.r-exams.org/templates/boxhist/).
- `boxplots`. *Práctico en R*. Pregunta de selección múltiple, sobre interpretación de un gráfico de boxplot. Es la versión en español traducida del ejemplo oficial de [r-exams.org](http://www.r-exams.org/templates/boxplots/).
- `regresion_lineal`. *Práctico en R*. Pregunta con múltiples enunciados, sobre análisis de regresión lineal. Es la versión en español traducida del ejemplo oficial de [r-exams.org](http://www.r-exams.org/templates/lm/).
