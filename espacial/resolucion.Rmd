```{r data generation, echo = FALSE, results = "hide", message=FALSE, warning=FALSE}
## data
fichero <- system.file("external/test.grd", package="raster")
a <- raster::raster(fichero)
fac <- sample(1:4, 1)
a <- raster::aggregate(a, fact = c(fac, fac), fun = mean)
ex <- paste0(names(sf::st_bbox(a)), ": ", sf::st_bbox(a), collapse = ", ")
exq <- glue::glue("`{ex}`")
## solution
res <- raster::res(a)[1]
```

Question
========
Conociendo que un raster en el sistema de coordenadas EPSG:9001 (Sistema proyectado, con medicion en metros) cubre una extensión máxima entre las coordenadas [`r exq`],y tiene `r nrow(a)` filas y `r ncol(a)` columnas. ¿Cuál es su resolución en el eje x?. Escriba el resultado con un solo decimal si es que tiene decimales.

Solution
========

La resolución es una propiedad espacial de los rasters que depende de la extensión máxima de la capa y la cantidad de filas o columnas. Un raster de una sola capa tiene dos resoluciones, uno por cada eje de coordenadas. En el caso propuesto la resolución para el exje `x` es `r res`, que resulta de dividir la diferencia entre xmin y xmax para el número de columnas

```{r echo=FALSE, results='asis'}
glue::glue(
    '{chki}
extension <- c({bbx})
cols <- 80
rows <- 115
(extension["xmax"] - extension["xmin"])/cols
{chko}
',
chki = paste0(paste(rep("`", 3), collapse = ""), "{r}"),
chko = paste(rep("`", 3), collapse = ""),
bbx = stringr::str_replace_all(ex, ":", " ="),
rows = nrow(a),
cols = ncol(a)
)
```

```{r echo=FALSE}
res
```

Cuando el raster está cargado en la sesión de R, se puede consultar usando la función `raster::res()`, de la siguiente forma 

```{r}
r <- raster::raster(system.file("external/test.grd", package="raster"))
raster::res(r)
```

...de estos dos valores resultantes, el primero corresponde a la resolución del eje `x`, mientras que el segundo corresponde al eje `y`. 

Meta-information
================
extype: num
exsolution: `r fmt(res, digits = 2)`
exname: Resolución espacial
extol: 0.01
