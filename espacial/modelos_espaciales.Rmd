Question
========
Uno de los elementos de la lista no es un modelo de datos espaciales. ¿Cuál es?

Answerlist
----------
* Raster
* Vector
* Redes de triángulos
* Mallas de puntos
* Cubos espacio-temporales
* Series temporales
* GPS
* Carta topográfica

Solution
========


Answerlist
----------
* Falso. Raster si es considerado como un modelo de datos para variables continuas en el espacio
* Falso. Vector es un modelo de datos para entidades discretas en el espacio
* Falso. Redes de triángulos son modelos espaciales para representar relaciones tridimensionales de las posiciones geográficas
* Falso. Mallas de puntos, son modelos de datos para representar variables continuas en el espacio, pero como puntos.
* Falso. Los cubos espacio temporales
* Verdadero. Las series temporales no son estructuras de datos espaciales, por lo tanto no puede ser considerada como un modelo de datos espaciales.
* Verdadero. El GPS es un dispositivo de calculo de posiciones geográficas.
* Verdadero. La carta topográfica es una composición final resultante del uso de modelos espaciales.


Meta-information
================
exname: Modelos espaciales
extype: schoice
exsolution: 00000111
exshuffle: 5
