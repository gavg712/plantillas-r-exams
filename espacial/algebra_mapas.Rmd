```{r data generation, echo = FALSE, results = "hide"}
dat <- c(
  "Local"  = "operación en la que solo interviene un pixel para calcular el valor",
  "Local"  = "operación se realiza independientemente en cada celda del espacio de trabajo",
  "Local"  = "operación del tipo `Raster_A + Raster_B`",
  "Focal"  = "operación en la cual el valor de un pixel depende de un vecindario",
  "Focal"  = "operación que requiere un vecindario para determinar el valor del pixel de interés",
  "Focal"  = "operación en el que el valor de un pixel está condicionado por los pixeles vecinos",
  "Zonal"  = "operación sobre los valores de un grupo de celdas contiguas que comparten una característica en común",
  "Zonal"  = "operación en la que el valor del pixel se repite para una zona de vecinos que comparten una caracteristica en común",
  "Zonal"  = "operación del tipo: _raster de NDVI promedio para cada rango de altitud_",
  "Global" = "operación en la que cada celda obtiene un valor en a partir de todas las celdas de la capa",
  "Global" = "operación en la que resulta un solo valor a partir de todos los valores de la capa",
  "Global" = "operación del tipo: _media del raster A_")
desc <- sample(dat, 1)
oper <- names(desc)
othe <- sample(dat[which(names(dat) != oper)], 4)
ejer <- sample(c(desc, othe), 5)
expl <- paste0(ifelse(names(ejer) == oper, "Felicitaciones! En efecto, esta ", "Esta "), 
               "es una operación de tipo ", names(ejer))
solu <- names(ejer) == oper
names(desc) <- NULL
```


Question
========
Complete la frase seleccionando la respuesta correcta: Una _operación `r oper`_ es una...

```{r questionlist, echo = FALSE, results = "asis"}
answerlist(ejer, markup = "markdown")
```

Solution
========

```{r solutionlist, echo = FALSE, results = "asis"}
answerlist(ifelse(solu, "Verdadero", "Falso"), expl, markup = "markdown")
```

Meta-information
================
exname: Álgebra de mapas
extype: schoice
exsolution: `r mchoice2string(solu, single = TRUE)`

