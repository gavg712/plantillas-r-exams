```{r data generation, echo = FALSE, results = "hide"}
suppressPackageStartupMessages(library(sf))
suppressPackageStartupMessages(library(tidyverse))
# data
f <- system.file("shape/nc.shp", package = "sf")
n <- sample(40:60, 1)
d <- st_sf(id = seq_len(n), 
           var = rweibull(n, 2.5, 1), 
           geometry = st_bbox(c(xmin = -79.4751, xmax = -77.9370, 
                                ymax = -3.3530, ymin = -1.8028), crs = st_crs(4326)) |>
             st_as_sfc() |> 
             st_transform(32717) |> 
             sf::st_sample(n))
d <- cbind(d, st_coordinates(d))
write.csv(st_drop_geometry(d), "points_data.csv", row.names = FALSE, quote = FALSE)

## QUESTION/ANSWER GENERATION
questions <- character(3)
solutions <- logical(3)
explanations <- character(3)
type <- character(3)

eje <- sample(c("X", "Y"), 1)
questions[1] <- paste0("El ", ifelse(eje == "X", "Easting (x)", "Northing (y)"), " de la media espacial de los puntos es:")
solutions[1] <- ifelse(eje == "X", mean(d$X), mean(d$Y))
explanations[1] <- paste0("La media espacial sin ponderar de los puntos es el par conformado por el promedio de sus coordenadas en cada eje. Por lo tanto el ", ifelse(eje == "X", "Easting (x)", "Northing (y)"), "de la media espacial de este dataset es: ", round(solutions[1], 2))
type[1] <- "num"


questions[2] <- "La Distancia Espacial Estándar de los puntos es:"
solutions[2] <- aspace2::sd_distance(st_drop_geometry(d)[,c("X", "Y")], output = "param")$radius
explanations[2] <- paste0("La Distancia Espacial Estándar sin ponderar de los puntos es el radio que representa la dispersión alrededor del centro medio. Para este caso la SSD es: ", round(solutions[2], 2))
type[2] <- "num"

eje <- sample(c("Semieje mayor", "Semieje menor", "ángulo de rotación", "excentricidad"), 1)
par <- aspace2::sd_ellipse(st_drop_geometry(d)[,c("X", "Y")], output = "param")
questions[3] <- paste0("La ellipse de desviación estándar tiene ", eje, " igual a:")
solutions[3] <- switch(eje, 
                       "Semieje mayor" = par$Sigma.x , 
                       "Semieje menor" = par$Sigma.y, 
                       "ángulo de rotación" = par$Theta, 
                       "excentricidad"  = par$Eccentricity)
unid <- switch(eje, 
               "Semieje mayor" =, 
               "Semieje menor" = "m", 
               "ángulo de rotación" = "º", 
               "excentricidad"  = "")
explanations[3] <- paste0("La ellipse de desviación estándar de los puntos es la forma elíptica que representa la dispersión alrededor del centro medio. Esta medida considera al menos dos ejes de dispersión, por lo tanto en este caso su ", eje, " es igual a ", round(solutions[3], 2), unid)
type[3] <- "num"
```    

Question
========
Usando los datos provistos en [points_data.csv](points_data.csv) (SRC = EPSG:`r st_crs(d)$epsg`) calcule las siguientes respuestas. _(NOTA: La tolerancia para las respuestas numéricas en esta pregunta es $\pm 0.05$.)_

```{r esda, echo = FALSE, results = "show", fig.height = 4.5, fig.width = 9, fig.path = "", fig.cap = ""}
mc <- st_drop_geometry(d) |>  summarise(X = mean(X), Y = mean(Y), col = "Media espacial")
ssd <- st_drop_geometry(d)[,c("X", "Y")] |>  aspace2::sd_distance()
sde <- st_drop_geometry(d)[,c("X", "Y")] |>  aspace2::sd_ellipse()

ggplot(d) +
  geom_sf(size = 0.5) +
  geom_point(data = mc, 
             aes(X, Y, color = col)) +
  geom_path(data = ssd$coord |> 
              mutate(col = "Distancia espacial estándar"), 
             aes(X, Y, color = col)) +
  geom_path(data = sde$coord |> 
              mutate(col = "Elipse de desviación estándar"), 
             aes(X, Y, color = col)) +
  coord_sf(crs = 32717,  datum = sf::st_crs(32717)) +
  theme_bw() +
  theme(axis.text.y = element_text(angle = 90))
```


```{r questionlist, echo = FALSE, results = "asis"}
exams::answerlist(unlist(questions), markup = "markdown")
```

Solution
========

Para replicar el análisis en R:

```
library(aspace)

## datos
d <- read.csv("points_data.csv") |> 
  st_as_sf(coords = c("X", "Y"), crs = 32717)
     
## centro medio
(mc <- apply(st_coordinates(d), 2, mean))

## Distancia espacial estándar
aspace::calc_sdd(points = st_coordinates(d))
sddatt

## Elipse de deviación estándar
aspace::calc_sde(points = st_coordinates(d))
sdeatt
```



```{r solutionlist, echo = FALSE, results = "asis"}
exams::answerlist(paste(unlist(explanations), ".", sep = ""), markup = "markdown")
```


Meta-information
================
exname: Análisis exploratorio I
extype: cloze
exsolution: `r paste(solutions, collapse = "|")`
exclozetype: `r paste(type, collapse = "|")`
extol: 0.05
