Lista de ejercicios en este directorio.

- `introducción`. *Teoría*. Pregunta de selección única sobre fundamentos de estadística espacial.
- `esda`. *Práctico en R*. Pregunta con múltiples enunciados, sobre análisis exploratorio de datos espaciales.
- `autocorrelacion_a`. *Práctico en R*. Pregunta con múltiples enunciados, sobre análisis de dependencia y patrones espaciales.
- `autocorrelacion_b`. *Teoría de estadística espacial*. Pregunta selección simple, sobre técnicas de análisis de dependencia y patrones espaciales.
- `spatial_lag`. *Práctico en R*. Pregunta con múltiples enunciados, sobre modelos de regresión lineal con desface espacial.
- `gwr`. *Práctico en R*. Pregunta con múltiples enunciados, sobre modelos de regresión lineal geográficamente ponderada.
- `geoestadistica`. *Teoría*. Pregunta de selección única sobre métodos de interpolación y geoestadística.
- `interpolacion`. *Práctica con R*. Pregunta se entrada de numérica simple, sobre interpolación con IDW.
- `interpolacion_2`. *Práctica con R*. Pregunta se entrada de numérica simple, sobre interpolación con IDW. Es similar a la anterior, pero con una mejor explicación y variaciones en `idp` y los datos.

