# Plantillas R-Exams

Colección de plantillas para [R-Exams](http://www.r-exams.org/) en español.

- [Estadística](estadistica)
- [Estadística_espacial](estadistica_espacial)
- [Espacial +  GIS](espacial)
- [Programación](programación)

## Usando las plantillas

Todas las plantillas son funcionales. Y tu puedes compilarlas para diferentes sistemas de evaluación.

- Moodle u OpenOLAT ([Ver tutorial](http://www.r-exams.org/tutorials/elearning/)) 
- Blackboard ([Ver tutorial](http://www.r-exams.org/tutorials/exams2blackboard/))
- Examen escrito ([Ver tutorial](http://www.r-exams.org/tutorials/nops_language/))
- Etc.

Ejemplo: De R-exams a Moodle

```r
# Descarga todas las plantillas en un directorio
library(exams)
preguntas <- list.files(path = "<directorio con plantillas>", 
                        pattern = "Rmd$", 
                        full.names = TRUE)
# Fijamos una semilla de aleatorización
set.seed(2022-02-22)

# Pasamod de Rmd a Moodle XML
exams2moodle(preguntas, 
             n = 10, # Número de repeticiones de cada pregunta
             name = "ID Examen", #Se creará un fichero con este nombre
             svg = TRUE
             )
# En Moodle, Importa el fichero XML que se crea al banco de preguntas del curso. 
```

## Cómo contribuir?

- [ ] Haz un [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) del repositorio.
- [Clona](https://docs.gitlab.com/ee/topics/git/getting_started.html#instantiate-workflow-with-clone) en un directorio local
- [ ] Edita y [Agrega](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) 
- Sube los cambios a tu fork con un [Push](https://docs.gitlab.com/ee/topics/git/getting_started.html#committing-workflow) y haz un [Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html#getting-started-with-merge-requests):

```
# Despues de hacer el fork
git clone https://gitlab.com/<tu usuario>/plantillas-r-exams.git

# edita el/los ficheros que desees y agrega al índice de git

git add <fichero>
git commit -m "<Mensaje informativo sobre el cambio>"

git push origin master
```

## Licencia
Este repositorio está licenciado como [GNU-GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html). Se asume que los contributores entienden y aceptan compartir sus cambios o adhesiones mediante la misma licencia.  


