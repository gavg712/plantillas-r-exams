```{r data generation, echo = FALSE, results = "hide"}
dat <- c(
  "Un _análisis estadístico_"  = "se puede dividir en dos partes: descriptivo e inferencial",
  "Un _análisis descriptivo_"  = "incluye la colección, la organización, la 
descripción y la presentación de datos.",
  "Un _análisis inferencial_"  = "se llega a un razonamiento lógico sobre el fenómeno a partir de mediante un test de hipótesis de los datos que lo representan",
  "La _estadística paramétrica_"  = "requiere mayor precisión y exactitud. Además, que los datos pertenezcan a una distribución normal y que la muestra sea de tamaño adecuado.",
  "La _estadística no paramétricas_"  = "requiere tests estadísticos menos exigentes, en donde condiciones como la de la normalidad de los datos no es indispensable.",
  "El _tamaño de muestra_"  = "es el número adecuado de muestras que garantizan minimizar errores y maximizar la confiablidad.",
  "En el _muestreo aleatorio_"  = "las muestras se realizan aleatoriamente en el área de estudio",
  "En el _muestreo sistemático_" = "las muestras se ubican de forma ordenada en el área de estudio",
  "En el _muestreo estratificado_" = "las muestras se ubican aletaria o regularmente por sub áreas dentro del área de estudio",
  "Una _hipótesis nula_" = " es aquella que niega un fenómeno, proceso o condición",
  "El _error tipo I_" = "se comete cuando creemos que la H0 es falsa, pero en realidad es verdadera",
  "El _error tipo II_" = "se comete cuando creemos que la H0 es verdadera, pero en realidad es falsa",
  "La _Estadística Espacial_" = "incluye los análisis de patrones de puntos, análisis de patrones regionales y modelos geoestadísticos.",
  "La _Geoestadística_" = "permite cuantificar la heterogeneidad espacial de variables."
)
desc <- sample(dat, 1)
oper <- names(desc)
othe <- sample(dat[which(names(dat) != oper)], 4)
ejer <- sample(c(desc, othe), 5)
expl <- ifelse(names(ejer) == oper, 
               "Felicitaciones, respuesta correcta ", 
               "Esta **NO** es la respuesta correcta!")
solu <- names(ejer) == oper
names(desc) <- NULL
```


Question
========
Complete la frase seleccionando la respuesta correcta: `r oper` ...

```{r questionlist, echo = FALSE, results = "asis"}
answerlist(ejer, markup = "markdown")
```

Solution
========

```{r solutionlist, echo = FALSE, results = "asis"}
answerlist(ifelse(solu, "Verdadero", "Falso"), expl, markup = "markdown")
```

Meta-information
================
exname: Estadística espacial
extype: schoice
exsolution: `r mchoice2string(solu, single = TRUE)`

