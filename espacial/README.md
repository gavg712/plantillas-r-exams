Lista de ejercicios en este directorio.

- `modelos_espaciales`. *Teoría de GIS*. Pregunta de selección única entre 5 posibles respuestas
- `funciones`. *Práctica con R*. Pregunta se entrada de texto simple, sobre funciones en R para lectura o manipulación de objetos espaciales.
- `crs`. *Práctica con R*. Consulta e interpretación de Sistema de Referencia de Coordenadas
- `resolución`. *Práctica con R*. Actividad para entender la estructura de un fichero Raster
- `entidades`. *Práctica con R*. Ejercicio de consulta de información de capas vectoriales en R.
- `crop`. *Práctica con R*. Ejercicio de recorte y agregación de información de capas vectoriales.
- `cellstats`. *Práctica con R*. Ejercicio de consulta de información estadística de los valores de un raster.
- `algebra_mapas`. *Teoría de GIS*. Pregunta sobre tipo de operaciones espaciales con capas raster. 

